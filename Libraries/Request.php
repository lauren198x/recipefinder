<?php
namespace Libraries;

class Request{

	private $_filter = null;

	function __construct()
	{

	}

	function applyFilter($filterName, $value)
	{
		if ($filterName) {
			if ($this->_filter===null) {
				if (class_exists('\Libraries\Filter')) {
					$this->_filter = new \Libraries\Filter;
				}
			}
			if ($this->_filter) {
				$filterMethod = $filterName.'Filter';
				if (method_exists($this->_filter, $filterMethod)) {
					return $this->_filter->$filterMethod($value);
				}
			}
		}
		return $value;
	}

	function getQuery($name, $filter=null, $default=null)
	{
		if (isset($_GET[$name])) {
			return $this->applyFilter($filter, $_GET[$name]);
		}
		return $default;
	}

	function getPost($name, $filter=null, $default=null)
	{
		if (isset($_POST[$name])) {
			return $this->applyFilter($filter, $_POST[$name]);
		}
		return $default;
	}

	function getRequest($name, $filter=null, $default=null)
	{
		if (isset($_REQUEST[$name])) {
			return $this->applyFilter($filter, $_REQUEST[$name]);
		}
		return $default;
	}
}