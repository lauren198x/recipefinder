<?php

namespace Libraries;

class Controller{

	protected $_DI;
	protected $_view;

	function __construct()
	{
		$this->_DI = \Libraries\DI::getInstance();
	}

	function get($serviceName)
	{
		return DI::get($serviceName);
	}

	function setParam($paramName, $paramValue)
	{
		if (!$this->_view) {
			$this->_view = $this->get('view');
		}

		$this->_view->setParam($paramName, $paramValue);
	}

	function view($template = null)
	{
		if (!$this->_view) {
			$this->_view = $this->get('view');
		}

		$this->_view->render($template);
	}

}
