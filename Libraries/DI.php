<?php

namespace Libraries;

class DI{
	protected static $_services = array();
	protected static $_instance;

	private function __construct()
	{

	}

	public static function getInstance()
	{
		$class = get_called_class();
		if (!isset(self::$_instance)) {
			self::$_instance = new $class();
		}
		return self::$_instance;
	}

	static function get($serviceName)
	{
		if (isset(self::$_services[$serviceName])) {
			return self::$_services[$serviceName];
		}
		return null;
	}

	static function set($serviceName, $value)
	{
		self::$_services[$serviceName] = $value;
		return self::$_services[$serviceName];
	}
}