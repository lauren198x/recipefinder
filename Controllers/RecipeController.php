<?php

namespace Controllers;

class RecipeController extends \Libraries\Controller
{
	public function indexAction()
	{
		$this->view();
	}

	public function finderAction()
	{
		$fridgeFile = isset($_FILES['fridge']) ? $_FILES['fridge'] : FALSE;
		$recipesFile = isset($_FILES['recipes']) ? $_FILES['recipes'] : FALSE;

		if ($fridgeFile['type'] !== 'text/csv' && $fridgeFile['type'] !== 'application/json') {
			$this->setParam('error', "Files type need to be .csv and .json");
			$this->view('index');
			return;
		}

		$fridge = new \Models\Fridge($fridgeFile['tmp_name']);
		$recipes = new \Models\Recipe($recipesFile['tmp_name']);

		$goodIngredients = $fridge->getGoodIngredient();
		$recipe = $recipes->getRecommendedRecipeFromStocks($goodIngredients);
		$this->setParam('recommendedRecipe', $recipe);
		$this->view('index');
	}
}