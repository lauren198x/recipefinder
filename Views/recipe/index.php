<form action="<?php echo $this->baseUrl('recipe/finder') ?>" enctype="multipart/form-data" method="post">
	<label for="fridge">Fridge content</label>
	<input type="file" id="fridge" name="fridge"><br>
	<label for="recipes">Recipes list</label>
	<input type="file" id="recipes" name="recipes"><br>
	<input type="submit">
</form>
<br>
<div>
<?php if (isset($error) && !empty($error)) :?>
	<p style="color:red;"><?php echo $error; ?></p>
<?php elseif (isset($recommendedRecipe)) : ?>
	<h2>Recommended Recipe</h2>
<?php
	if (sizeof($recommendedRecipe)) {
		$recipe = $recommendedRecipe;
?>
	<h3><?php echo $recipe['name'] ?></h3>
	<h5>Ingredients:</h5>
	<ul>
		<?php foreach ($recipe['ingredients'] as $ingredient) : ?>
		<li><?php echo $ingredient['amount'] ?> <?php echo $ingredient['unit'] ?>, <?php echo $ingredient['item'] ?></li>
		<?php endforeach; ?>
	</ul>
<?php
	} else {
		echo "<p>Order Takeout</p>";
	}
?>
<?php endif; ?>
</div>