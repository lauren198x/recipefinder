<?php

namespace Models;

class Recipe extends \Libraries\Model
{
	private $_recipes = array();

	function __construct($file= null)
	{
		if ($file) {
			$this->parseJSON($file);
		}
	}

	/**
	 * Fridge recipes setter
	 */
	function setRecipes($arr = array())
	{
		$this->_recipes = $arr;
	}

	/**
	 * Fridge recipes setter
	 */
	function getRecipes()
	{
		return $this->_recipes;
	}

	/**
	 * Parsing uploaded json file into array
	 * @param $file
	 */
	function parseJSON($file)
	{
		$recipes = json_decode(file_get_contents($file, TRUE), TRUE);
		$this->setRecipes($recipes);
	}

	function getRecipesCount()
	{
		return count($this->getRecipes());
	}

	/**
	 * Function to match the stocks and recipes libraries
	 * @param $stocks Array
	 *
	 * @return $matches Array
	 */
	function getMatchStockRecipe($stocks = array())
	{
		$matches = array();
		$recipes = $this->getRecipes();
		if (sizeof($recipes) && sizeof($stocks)) {
			foreach ($recipes as $recipe) {
				$needMatches = count($recipe['ingredients']);
				$matchesIngredient = 0;
				$closestUseby = 0;
				foreach ($recipe['ingredients'] as $ingredient) {
					foreach ($stocks as $stock) {
						if ($ingredient['item'] == $stock['item'] && $ingredient['amount'] <= $stock['amount']) {
							if ($closestUseby == 0 || $closestUseby > $stock['useby']) {
								$closestUseby = $stock['useby'];
							}
							$matchesIngredient++;
						}
					}
				}

				if ($matchesIngredient == $needMatches) {
					$matches[] = $recipe + array('closestUseby' => $closestUseby);
				}
			}
		}
		return $matches;
	}

	/**
	 * Function to get recommended recipe from list of recipes by closestUseby datetime
	 * @param $recipes Array
	 * @return $recipe
	 */
	function getRecommendedRecipe($recipes = array())
	{
		$recommendedRecipe = null;
		if (sizeof($recipes)) {
			foreach ($recipes as $recipe) {
				if ($recommendedRecipe==null || $recommendedRecipe['closestUseby'] > $recipe['closestUseby']) {
					$recommendedRecipe = $recipe;
				}
			}
		}

		return $recommendedRecipe;
	}

	/**
	 * Function to get recommended recipe from provided stocks
	 * @param $stocks Array
	 * @return $recipe
	 */
	function getRecommendedRecipeFromStocks($stocks = array())
	{
		$matchesRecipe = $this->getMatchStockRecipe($stocks);

		return $this->getRecommendedRecipe($matchesRecipe);
	}
}