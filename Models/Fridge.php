<?php

namespace Models;

class Fridge extends \Libraries\Model
{
	private $_today;
	private $_contents = array();
	private $_expired = array();
	private $_good = array();
	private $_UNIT = array(
		'kilograms'  => 0, 
		'grams'      => 1, 
		'gram'       => 1, 
		'ml'         => 2, 
		'mililiters' => 2, 
		'slices'     => 3, 
		'of'         => 4
	);

	function __construct($file = null)
	{
		$this->_today = strtotime(date('d-m-Y'));
		if ($file) {
			$this->parseCSV($file);
		}
	}

	private function _unit($unit)
	{
		return $this->_UNIT[$unit];
	}

	/**
	 * Fridge content setter
	 */
	function setContents($arr = array())
	{
		$this->_contents = $arr;
	}

	/**
	 * Fridge content getter
	 */
	function getContents()
	{
		return $this->_contents;
	}

	function addExpiredContent($content = array())
	{
		$this->_expired[] = $content;
	}

	function addGoodContent($content = array())
	{
		$this->_good[] = $content;
	}

	/**
	 * Parsing uploaded CSV file into array
	 * @param $file
	 */
	function parseCSV($file)
	{
		$csv = array_map('str_getcsv', file($file));
		$contents = array();
		foreach ($csv as &$content) {

			$_content = array(
				'item'   => $content[0],
				'amount' => $content[1],
				'unit'   => $this->_unit($content[2]),
				'useby'  => strtotime(str_replace('/', '-', $content[3]))
			);

			if ($this->_today > $_content['useby']) {
				$this->addExpiredContent($_content);
			} else {
				$this->addGoodContent($_content);
			}

			$contents[] = $_content;
		}
		$this->setContents($contents);
	}

	function getContentsCount()
	{
		return count($this->getContents());
	}

	function getExpiredIngredient()
	{
		return $this->_expired;
	}

	function getExpiredIngredientCount()
	{
		return count($this->getExpiredIngredient());
	}

	function getGoodIngredient()
	{
		return $this->_good;
	}

	function getGoodIngredientCount()
	{
		return count($this->getGoodIngredient());
	}

	function getTodayTime()
	{
		return $this->_today;
	}
}