<?php

use \Libraries\DI as DI;

class Application {

	var $services = array();

	function __construct()
	{
	}

	function set($serviceName, $value)
	{
		return DI::set($serviceName, $value);
	}

	function get($serviceName)
	{
		return DI::get($serviceName);
	}

	function handleRequest()
	{
		if (!$this->get('request')) {
			DI::set('request', new \Libraries\Request());
		}
		if (!$this->get('dispatcher')) {
			DI::set('dispatcher', new \Libraries\Dispatcher());
		}
		if (!$this->get('view')) {
			DI::set('view', new \Libraries\View());
		}
		if (!$this->get('url')) {
			DI::set('url', new \Helpers\Url());
		}

		$request = $this->get('request');
		$dispatcher = $this->get('dispatcher');
		$url = $this->get('url');

		$dispatcher->setPath($url->getPath());

		$dispatcher->dispatch();

	}

}