<?php

namespace Helpers;

class Url{

	private $_currentUrl;
	private $_baseUrl;
	private $_url;

	function __construct()
	{
		$this->_url = ltrim(\Libraries\DI::get('request')->getQuery('_url'), '/');
	}

	function getPath()
	{
		return $this->_url;
	}

	function getCurrentUrl()
	{
		if (!$this->_currentUrl) {
			$this->_currentUrl = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		}
		return $this->_currentUrl;
	}

	function getBaseUrl($path = null)
	{
		if (!$this->_baseUrl) {
			$this->_baseUrl = str_replace($this->_url, '', $this->getCurrentUrl());
		}

		return $this->_baseUrl.ltrim($path, '/');
	}

	function __get($name)
	{
		$method = 'get'.ucfirst($name);
		if (method_exists($this, $method)) {
			$args = func_get_args();
			$this->$method($args);
		} else {
			 $trace = debug_backtrace();
			trigger_error(
				'Undefined property via __get(): ' . $name .
				' in ' . $trace[0]['file'] .
				' on line ' . $trace[0]['line'],
				E_USER_NOTICE);
			return null;
		}
	}
}